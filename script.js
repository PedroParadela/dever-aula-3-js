
// Deixando o "Digite a mensagem..."
resetInput()


// Evento que faz o "Digite a mensagem" sumir
const msg = document.querySelector(".input-msg")
msg.addEventListener("click", (event) => {
    if (msg.value === "Digite a mensagem...") {
        msg.style.color = "black"
        msg.value = ""
    }
}) 


// Evento para enviar com o enter:
msg.addEventListener("keydown", (event) => {
    if (event.keyCode === 13) {
        event.preventDefault(true)
        send()
    }
})


// Criando o botão Enviar
const localBotao = document.querySelector(".entrada")
localBotao.appendChild(createButton("Enviar", "btn-enviar", "#0066CC", "#73b9ff", "white", button => {
    
    // Clicar no botão
    button.addEventListener("click", event => {
        send()
        resetInput()
    })
}, "10px 20px"))


// Função que cria os botões de excluir e editar:
function createEdEx(div) {
    div.appendChild(createButton("Editar", "btn-ed", "#F6A75D", "#ffc894", "black", button => button.addEventListener("click", event => {
        const parent = button.parentElement
        const button2 = parent.lastChild

        button.remove()
        button2.remove()

        inputEditar = document.createElement("input")
        inputEditar.classList.add("input-editar")
        inputEditar.addEventListener("keydown", event => {
            if (event.keyCode === 13) {
                event.preventDefault(true)
                save()
            }
        })

        parent.appendChild(inputEditar)
        parent.appendChild(createButton("Salvar", "btn-salvar", "rgb(44, 202, 44)", "rgb(164, 255, 164)", "white", button => button.addEventListener("click", event => save())))
    })))
    div.appendChild(createButton("Excluir", "btn-ex", "#F65D5D", "#ff8989", "black", button => button.addEventListener("click", event => deletar(button))))
}


// Função que envia a mensagem
function send() {
    const input = document.querySelector(".input-msg")

    if (input.value != "") {
        const mensagem = document.createElement("li")
    
        // Criando a mensagem
        mensagem.innerHTML = `<p class="texto">${input.value}</p>
            <div class="botoes">
            </div>`
    
        input.value = ""
    
        // Criando os botões
        const div = mensagem.lastChild
        createEdEx(div)
    
        const ul = document.querySelector(".historico")
        ul.appendChild(mensagem)
    }
}


// Função de editar
function save() {
    const input = document.querySelector(".input-editar")
    const parent = input.parentElement
    const mensagem = input.parentElement.parentElement.firstChild

    mensagem.innerText = input.value
    
    parent.removeChild(parent.lastChild)
    
    // Criando os botões
    const div = parent
    createEdEx(div)
    
    parent.removeChild(input)
}


// Função de criar botão
function createButton(text="Enviar", classe=text, background="white", backgroundHover="white", color="black", callback, padding="5px") {
    const botao = document.createElement('button')
    botao.innerText = text
    botao.classList.add(classe)

    botao.style.cssText = `
        background-color: ${background};
        color: ${color};
        padding: ${padding};
    `

    botao.addEventListener("mouseover", event => event.target.style.background = backgroundHover)

    botao.addEventListener("mouseout", event => event.target.style.background = background)
    
    try{
        callback(botao)
    } catch(err){

    }

    return botao
}

function deletar(elemento) {
    const parent = elemento.parentElement.parentElement
    parent.remove();
}

function resetInput() {
    // Digite a mensagem
    const msg = document.querySelector(".input-msg")
    msg.value = "Digite a mensagem..."
    msg.style.color = "#969696"
}
